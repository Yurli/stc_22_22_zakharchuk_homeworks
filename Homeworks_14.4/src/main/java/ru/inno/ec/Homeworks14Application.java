package ru.inno.ec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homeworks14Application {

    public static void main(String[] args) {
        SpringApplication.run(Homeworks14Application.class, args);
    }

}
