package ru.inno.ec.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.ec.models.Course;
import ru.inno.ec.services.CoursesService;

import java.util.List;

@Controller
public class CoursesController {
    private final CoursesService coursesService;

    @Autowired
    public CoursesController(CoursesService coursesService) {
        this.coursesService = coursesService;
    }

    @PostMapping("/courses/{course-id}/students")
    public String addStudentToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("student-id") Long studentId) {
        coursesService.addStudentToCourse(courseId, studentId);
        return "redirect:/courses/" + courseId;
    }

    @GetMapping("/courses/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(courseId));
        model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(courseId));
        return "course_page";
    }

    @GetMapping("/courses")
    public String getCoursesPage(Model model) {
        List<Course> courses = coursesService.getAllCourses();
        model.addAttribute("courses", courses);
        return "all_course_page";
    }

    @PostMapping("/courses")
    public String addCourse(Course course) {
        coursesService.addCourse(course);
        return "redirect:/courses";
    }

    @PostMapping("/courses/{course-id}/update")
    public String updateCourse(@PathVariable("course-id") Long courseId, Course course) {
        coursesService.updateCourse(courseId, course);
        return "redirect:/courses/" + courseId;
    }

    @GetMapping("/courses/{course-id}/delete")
    public String updateCourse(@PathVariable("course-id") Long courseId) {
        coursesService.deleteCourse(courseId);
        return "redirect:/courses";
    }
}