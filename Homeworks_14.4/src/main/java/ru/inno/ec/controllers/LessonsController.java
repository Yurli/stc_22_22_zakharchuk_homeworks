package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.services.CoursesService;
import ru.inno.ec.services.LessonsService;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class LessonsController {
    private final LessonsService lessonsService;
    private final CoursesService coursesService;

    @GetMapping("/lessons")
    public String getLessonsPage(Model model) {
        List<Lesson> lesson = lessonsService.getAllLessons();
        model.addAttribute("lessons", lesson);
        return "lessons_page";
    }

    @GetMapping("/lessons/{lesson-id}")
    public String getLessonPage(@PathVariable("lesson-id") Long id, Model model) {
        model.addAttribute("lesson", lessonsService.getLesson(id));
        return "lesson_page";
    }

    @PostMapping("/lessons")
    public String addLessons(Lesson lesson) {
        lessonsService.addLesson(lesson);
        return "redirect:/lessons";
    }

    @PostMapping("/lessons/{lesson-id}/update")
    public String updateLessons(@PathVariable("lesson-id") Long lessonId, Lesson lesson) {
        lessonsService.updateLessons(lessonId, lesson);
        return "redirect:/lessons/" + lessonId;
    }

    @GetMapping("/lessons/{lesson-id}/delete")
    public String updateLessons(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteCourse(lessonId);
        return "redirect:/lessons";
    }
}

