import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;

    public CarsRepositoryFileBasedImpl(String listOfCars) {
        this.fileName = listOfCars;
    }

    private static final Function<String, Cars> stringToCarsMapper = currentUser -> {
        String[] parts = currentUser.split("\\|");
        String carNumber = parts[0];
        String carBrand = parts[1];
        String carColor = parts[2];
        Integer carMileage = Integer.parseInt(parts[3]);
        Integer carCost = Integer.parseInt(parts[4]);
        return new Cars(carNumber, carBrand, carColor, carMileage, carCost);
    };
    private static final Function<Cars, String> userToStringMapper = Cars ->
            Cars.getCarNumber() + "|" + Cars.getCarBrand() + "|" + Cars.getCarColor() + "|" + Cars.getCarMileage() + "|"
                    + Cars.getCarCost();


    @Override
    public int findCarPriceRange(Integer from, Integer to) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarsMapper)
                    .filter(cars -> cars.getCarCost() >= from && cars.getCarCost() <= to).toList()
                    .size();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public String findColorByCarCost() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarsMapper)
                    .min(Comparator.comparingInt(Cars::getCarCost))
                    .get()
                    .getCarColor();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public double findAverageCostCar() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarsMapper)
                    .mapToInt(Cars::getCarCost)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
