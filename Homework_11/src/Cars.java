public class Cars {
    private String carNumber;
    private String carBrand;
    private String carColor;
    private Integer carMileage;
    private Integer carCost;

    public Cars(String carNumber, String carBrand, String carColor, Integer carMileage, Integer carCost) {
        this.carNumber = carNumber;
        this.carBrand = carBrand;
        this.carColor = carColor;
        this.carMileage = carMileage;
        this.carCost = carCost;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String getCarColor() {
        return carColor;
    }

    public Integer getCarMileage() {
        return carMileage;
    }

    public Integer getCarCost() {
        return carCost;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "carNumber='" + carNumber + '\'' +
                ", carBrand='" + carBrand + '\'' +
                ", carColor='" + carColor + '\'' +
                ", carMileage=" + carMileage +
                ", carCost=" + carCost +
                '}';
    }
}

