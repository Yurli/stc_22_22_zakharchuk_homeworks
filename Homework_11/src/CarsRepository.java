public interface CarsRepository {
    int findCarPriceRange(Integer from, Integer to);

    String findColorByCarCost();

    double findAverageCostCar();
}