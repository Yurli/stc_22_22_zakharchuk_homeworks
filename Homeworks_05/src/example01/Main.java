package example01;
import java.util.Scanner;



public class Main {
    public static void main(String[] args) {
        Atm atm = new Atm(10000, 90000);
        while (true) {
            System.out.println("***************\nЧто будем делать?\n Нажмите - 1 для снятия. \n Нажмите - 2 для пополнения.\n Нажмите 0 для завершения. ");
            System.out.println(" Выберете действие");
            Scanner scanner = new Scanner(System.in);
            int numbers = scanner.nextInt();
            if (numbers == 1) {
                System.out.println(" Введите сумму получения");
                atm.outcush((scanner.nextDouble()));
                continue;
            }
            if (numbers == 2) {
                System.out.println(" Введите сумму взноса");
                atm.putOnMoney(scanner.nextDouble());
                continue;
            }
            if (numbers == 0) {
                System.out.println("Хорошего дня!!!");
                break;
            }
        }
        System.out.println("Проведено операций:  " + atm.getTransactionCounter());
        System.out.println(atm.getCurrentSumInAtm());
    }

}
