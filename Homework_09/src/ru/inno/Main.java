package ru.inno;

public class Main {

    public static void main(String[] args) {
        List<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(8);
        integerArrayList.add(10);
        integerArrayList.add(11);
        integerArrayList.add(13);
        integerArrayList.add(11);
        integerArrayList.add(15);
        integerArrayList.add(-5);


        System.out.println("\nArrayList");
        printCollection(integerArrayList);
        integerArrayList.removeAt(2);
        integerArrayList.remove(8);
        integerArrayList.remove(3);
        System.out.println("\nnewArrayList");
        printCollection(integerArrayList);




    }

    private static void printCollection(List<Integer> integerArrayList) {
        for (int i = 0; i < integerArrayList.size(); i++) {
            System.out.print(integerArrayList.get(i) + " ");
        }
    }


}