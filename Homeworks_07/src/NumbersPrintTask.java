public abstract class NumbersPrintTask implements Task {

    int from;
    int to;

    public NumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void complete() {

    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
}
