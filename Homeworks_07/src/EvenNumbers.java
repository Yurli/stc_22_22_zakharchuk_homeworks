public class EvenNumbers extends NumbersPrintTask {
    public EvenNumbers(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        for (int i = getFrom(); i <= getTo(); i++) {
            if (i % 2 == 0)
                System.out.println("Число" + " " + i + " " + "четное");

        }
    }
}
