public class Main {
    public static void main(String[] args) {
        Task t1 = new EvenNumbers(1,19);
        Task t2 = new OddNumbers(19,47);
        completeAllTasks(new Task[]{t1,t2});
    }
    public static void completeAllTasks(Task[] tasks) {
        for (Task t: tasks) {
            t.complete();
        }

    }
}