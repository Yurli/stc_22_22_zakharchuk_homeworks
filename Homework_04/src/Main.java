import javax.sql.rowset.serial.SQLOutputImpl;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static boolean isFunction(int sum) {
        return sum != 0;
    }

    public static void printEvanNumbers(int a[], int i) {
        if (a[i] % 2 == 0) {
            System.out.println(a[i]);
        }
    }

    public static int toInt(int[] array, int result) {
        for (int i = array.length - 1, n = 0; i >= 0; --i, n++) {
            int pos = (int) Math.pow(10, i);
            result += array[n] * pos;
        }
        return result;
    }

    public static String toInt(int[] array, String result) {
        for (int i = 0; i < array.length; i++) {
            result += array[i];
        }
        return result;
    }

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ширину массива");
        int Length = scanner.nextInt();
        System.out.println("Введите массив");
        int[] array = new int[Length];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
            printEvanNumbers(array, i);
        }
        int sum = calcSumOfArrayRange(array, 0, array.length - 1);
        if (isFunction(sum)) {
            System.out.println("Сумма  " + sum);
        } else {
            System.out.println("Значение:  -1");
        }
        System.out.println(toInt(array, ""));
        System.out.println(toInt(array, 0));
    }
}
