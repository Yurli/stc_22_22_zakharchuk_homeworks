package ru.inno.repository.impl;

import ru.inno.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

public class CarRepositoryJdbcImpl implements ru.inno.repository.CarRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into car (model, colour, number) " +
            "values (?, ?, ?)";

    private DataSource dataSource;

    public CarRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    private static final Function<ResultSet, Car> userRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getLong("id"))
                    .model(row.getString("model"))
                    .colour(row.getString("colour"))
                    .number(row.getString("number"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };



    @Override
    public List<Car> read() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = userRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return cars;

    }

    @Override
    public void write(Car car) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColour());
            preparedStatement.setString(3, car.getNumber());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert student");
            }
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {

                if (generatedId.next()) {
                    car.setId(generatedId.getLong("id"));
                } else {
                    // если сдвинуть итератор не получилось, выбрасываем исключение
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

    }




    }


