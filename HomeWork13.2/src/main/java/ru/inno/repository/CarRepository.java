package ru.inno.repository;

import ru.inno.models.Car;

import java.util.List;

public interface CarRepository {
    List<Car> read();

    void write(Car car);



}
