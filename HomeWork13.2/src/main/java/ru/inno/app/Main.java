package ru.inno.app;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.models.Car;
import ru.inno.repository.CarRepository;

import ru.inno.repository.impl.CarRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Properties;
import java.util.Scanner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;




public class Main {
    @Parameter(names = {"--action"})
    private String action;


    public static void main(String[] args) {
        Main program = new Main();
        Scanner scanner = new Scanner(System.in);
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/application.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));




        JCommander.newBuilder()
                .addObject(program)
                .build()
                .parse(args);

        CarRepository carRepository = new CarRepositoryJdbcImpl(dataSource);

        if (program.action.equalsIgnoreCase("read")) System.out.print(carRepository.read());
        if (program.action.equalsIgnoreCase("exit")) return;

        if (program.action.equalsIgnoreCase("write"));
        {
            //Scanner scanner = new Scanner(System.in);
            System.out.println("Введите модель");
            String model = scanner.nextLine();
            System.out.println("Введите цвет");
            String colour = scanner.nextLine();
            System.out.println("Введите номер");
            String number = scanner.nextLine();
            Car car = Car.builder()
                    .model(model)
                    .colour(colour)
                    .number(number)
                    .build();

            carRepository.write(car);

        }



    }


}

