import java.io.IOException;

public class UnsuccessfulWorkWithFileException extends RuntimeException {
    public UnsuccessfulWorkWithFileException(Exception e) {
        super(e);
    }
}