import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt (parts[0]);
        String nameProduct = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer inStock = Integer.parseInt(parts[3]);
        return new Product (id, nameProduct, cost, inStock);
    };


    @Override
    public List<Product> findProductById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> Objects.equals(product.getId(), id))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }
}