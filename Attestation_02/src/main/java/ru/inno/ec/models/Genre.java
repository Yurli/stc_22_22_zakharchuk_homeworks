package ru.inno.ec.models;


import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"lessons", "accounts"})
@Entity
public class Genre {
    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @Column(length = 1000)
    private String description;

    @OneToMany(mappedBy = "genre", fetch = FetchType.EAGER)
    private Set<Film> films;

    @ManyToMany(mappedBy = "genres", fetch = FetchType.EAGER)
    private Set<User> accounts;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
