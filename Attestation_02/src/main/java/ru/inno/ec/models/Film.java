package ru.inno.ec.models;


import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "genre")
@ToString(exclude = "genre")
public class Film {
    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Column(length = 1000)
    private String summary;


    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
