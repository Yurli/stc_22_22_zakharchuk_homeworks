package ru.inno.ec.services;

import ru.inno.ec.models.Genre;
import ru.inno.ec.models.User;

import java.util.List;

public interface GenresService {
    void addAccountToGenre(Long genreId, Long accountId);

    Genre getGenre(Long genreId);

    List<User> getNotInGenreAccounts(Long genreId);

    List<User> getInGenreAccounts(Long genreId);

    List<Genre> getAllGenres();

    void addGenre(Genre genre);

    void updateGenre(Long courseId, Genre genre);

    void deleteGenre(Long courseId);
}