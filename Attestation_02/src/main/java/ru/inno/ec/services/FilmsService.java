package ru.inno.ec.services;

import ru.inno.ec.models.Film;

import java.util.List;

public interface FilmsService {
    List<Film> getAllFilms();

    void addFilms(Film film);

    Film getFilms(Long id);


    void updateFilms(Long lessonId, Film film);

    void deleteGenre(Long lessonId);
}

