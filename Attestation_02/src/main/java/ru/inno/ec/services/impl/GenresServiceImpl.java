package ru.inno.ec.services.impl;


import org.springframework.stereotype.Service;
import ru.inno.ec.models.Genre;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.GenresRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.GenresService;

import java.util.List;

@Service
public class GenresServiceImpl implements GenresService {

    private final GenresRepository genresRepository;
    private final UsersRepository usersRepository;

    public GenresServiceImpl(GenresRepository genresRepository, UsersRepository usersRepository) {
        this.genresRepository = genresRepository;
        this.usersRepository = usersRepository;
    }


    @Override
    public void addAccountToGenre(Long genreId, Long accountId) {
        Genre genre = genresRepository.findById(genreId).orElseThrow();
        User account = usersRepository.findById(accountId).orElseThrow();
        account.getGenres().add(genre);
        usersRepository.save(account);


    }

    @Override
    public Genre getGenre(Long genreId) {
        return genresRepository.findById(genreId).orElseThrow();
    }

    @Override
    public List<User> getNotInGenreAccounts(Long genreId) {
        Genre genre = genresRepository.findById(genreId).orElseThrow();
        return usersRepository.findAllByGenresNotContains(genre);
    }

    @Override
    public List<User> getInGenreAccounts(Long genreId) {
        Genre genre = genresRepository.findById(genreId).orElseThrow();
        return usersRepository.findAllByGenresContains(genre);
    }

    @Override
    public List<Genre> getAllGenres() {
        return genresRepository.findAllByStateNot(Genre.State.DELETED);
    }

    @Override
    public void addGenre(Genre genre) {
        Genre newGenre = Genre.builder()
            .title(genre.getTitle())
            .description(genre.getDescription())
            .state(Genre.State.NOT_CONFIRMED)
            .build();
        genresRepository.save(newGenre);

    }

    @Override
    public void updateGenre(Long courseId, Genre updateData) {
        Genre genreForUpdate = genresRepository.findById(courseId).orElseThrow();
        genreForUpdate.setTitle(updateData.getTitle());
        genreForUpdate.setDescription(updateData.getDescription());
        genresRepository.save(genreForUpdate);

    }

    @Override
    public void deleteGenre(Long courseId) {
        Genre genreForDelete = genresRepository.findById(courseId).orElseThrow();
        genreForDelete.setState(Genre.State.DELETED);
        genresRepository.save(genreForDelete);

    }
}



