package ru.inno.ec.services.impl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Film;
import ru.inno.ec.repositories.GenresRepository;
import ru.inno.ec.repositories.FilmsRepository;
import ru.inno.ec.services.FilmsService;
import java.util.List;

@Service
@RequiredArgsConstructor
@Data
public class FilmsServiceImpl implements FilmsService {
    private final FilmsRepository filmsRepository;
    private final GenresRepository genresRepository;

    @Override
    public List<Film> getAllFilms() {
        return filmsRepository.findAllByStateNot(Film.State.DELETED);
    }

    @Override
    public Film getFilms(Long id) {
        return filmsRepository.findById(id).orElseThrow();
    }

    @Override
    public void addFilms(Film film) {
        Film newFilm = Film.builder()
                .name(film.getName())
                .summary(film.getSummary())
                .state(Film.State.NOT_CONFIRMED)
                .build();
        filmsRepository.save(newFilm);

    }

    @Override
    public void updateFilms(Long lessonId, Film updateData) {
        Film filmForUpdate = filmsRepository.findById(lessonId).orElseThrow();
        filmForUpdate.setName(updateData.getName());
        filmForUpdate.setSummary(updateData.getSummary());
        filmsRepository.save(filmForUpdate);

    }

    @Override
    public void deleteGenre(Long lessonId) {
        Film filmForDelete = filmsRepository.findById(lessonId).orElseThrow();
        filmForDelete.setState(Film.State.DELETED);
        filmsRepository.save(filmForDelete);
    }
}
