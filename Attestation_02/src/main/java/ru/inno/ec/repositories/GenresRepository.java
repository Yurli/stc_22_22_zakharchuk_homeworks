package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Genre;

import java.util.List;

public interface GenresRepository extends JpaRepository<Genre, Long> {

    List<Genre> findAllByStateNot(Genre.State deleted);
}
