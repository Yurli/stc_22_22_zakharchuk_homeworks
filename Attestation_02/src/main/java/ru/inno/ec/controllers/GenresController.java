package ru.inno.ec.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.ec.models.Genre;
import ru.inno.ec.security.deteils.CustomUserDetails;
import ru.inno.ec.services.GenresService;

import java.util.List;

@Controller
public class GenresController {
    private final GenresService genresService;

    @Autowired
    public GenresController(GenresService genresService) {
        this.genresService = genresService;
    }

    @PostMapping("/admin/genres/{genre-id}/account")
    public String addAccountToGenre(@PathVariable("genre-id") Long genreId,
                                    @RequestParam("account-id") Long accountId) {
        genresService.addAccountToGenre(genreId, accountId);
        return "redirect:/genres/" + genreId;
    }

    @GetMapping("/genres/{genre-id}")
    public String getGenrePage(@PathVariable("genre-id") Long genreId, Model model) {
        model.addAttribute("genres", genresService.getGenre(genreId));
        model.addAttribute("notInGenreAccount", genresService.getNotInGenreAccounts(genreId));
        model.addAttribute("inGenreAccounts", genresService.getInGenreAccounts(genreId));
        return "genre_page";
    }

    @GetMapping("/genres")
    public String getGenresPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        List<Genre> genres = genresService.getAllGenres();
        model.addAttribute("genres", genres);
        return "genres_page";
    }

    @PostMapping("/genres")
    public String addGenre(Genre genre) {
        genresService.addGenre(genre);
        return "redirect:/genres";
    }

    @PostMapping("/genres/{genre-id}/update")
    public String updateGenre(@PathVariable("genre-id") Long genreId, Genre genre) {
        genresService.updateGenre(genreId, genre);
        return "redirect:/genres/" + genreId;
    }

    @GetMapping("/genres/{genre-id}/delete")
    public String updateGenre(@PathVariable("genre-id") Long genreId) {
        genresService.deleteGenre(genreId);
        return "redirect:/genres";
    }
}