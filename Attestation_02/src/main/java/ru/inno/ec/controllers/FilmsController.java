package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.ec.models.Film;
import ru.inno.ec.security.deteils.CustomUserDetails;
import ru.inno.ec.services.GenresService;
import ru.inno.ec.services.FilmsService;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class FilmsController {
    private final FilmsService filmsService;
    private final GenresService genresService;

    @GetMapping("/films")
    public String getFilmsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        List<Film> film = filmsService.getAllFilms();
        model.addAttribute("films", film);
        return "films_page";
    }

    @GetMapping("/films/{film-id}")
    public String getFilmPage(@PathVariable("film-id") Long id, Model model) {
        model.addAttribute("film", filmsService.getFilms(id));
        return "film_page";
    }

    @PostMapping("/films")
    public String addFilms(Film film) {
        filmsService.addFilms(film);
        return "redirect:/films";
    }

    @PostMapping("/films/{film-id}/update")
    public String updateFilms(@PathVariable("film-id") Long filmId, Film film) {
        filmsService.updateFilms(filmId, film);
        return "redirect:/films/" + filmId;
    }

    @GetMapping("/films/{film-id}/delete")
    public String updateFilms(@PathVariable("film-id") Long filmId) {
        filmsService.deleteGenre(filmId);
        return "redirect:/films";
    }
}

