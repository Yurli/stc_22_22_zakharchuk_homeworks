import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String line = "Hello Hello bye Hello bye Inno";
        String[] test = line.split("[\\s\\xA0]+");
        System.out.println(test.length);
        for (String s : test) {
            System.out.println(s);
        }
        Map<String, Integer> mp = new HashMap<String, Integer>();

        for (String string : test) {

            if (mp.keySet().contains(string)) {
                mp.put(string, mp.get(string) + 1);

            } else {
                mp.put(string, 1);
            }
        }

        System.out.println(mp);
    }

}


