public class Main {
    public static void main(String[] args) {
        int[] array = {12, 62, 4, 2, 100, 40, 56};

        ArrayTask resolve2 = (array1, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            System.out.println("Calk Sum Max Element Number : \n " + sum);
            return sum;
        };
        ArrayTask resolve3 = (array1, from, to) ->{
            int i = from, right = 0, left = 0, max = 0, localMax = max;
            while (i <= to) {
                left = max;
                max = right;
                right = array[i++];
                if (right < max && max > left) {
                    System.out.println("local max: \n" + max);
                    localMax = max;
                }
            }
            int num = localMax;
            int sum = 0;
            while (num > 0) {
                sum = sum + num % 10;
                num = num / 10;
            }
            System.out.println("Sum Max Element Number : \n"+ sum);
            return sum;
        };
        ArraysTasksResolver.resolveTask(array,resolve2, 1,5);
    }


}
