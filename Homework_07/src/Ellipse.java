public class Ellipse extends Сircle {
    protected double bigRadius;
    public Ellipse(double coordinateX, double coordinateY, double radius, double bigRadius) {
        super(coordinateX, coordinateY, radius);
        this.bigRadius = bigRadius;
    }
    public double areaEllipse() {
        return (int) (getRadius() * getBigRadius() * getPi());
    }
    public double perimeterEllipse() {
        return (int) (getPi() * (getRadius() + getBigRadius()));
    }
    public double getBigRadius() {
        return bigRadius;
    }
}



