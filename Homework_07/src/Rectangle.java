public class Rectangle extends Square {
    protected double width;
    public Rectangle(int coordinateX, int coordinateY, int height, int width) {
        super(coordinateX, coordinateY, height);
        this.width = width;
    }
    public double areaRectangle() {
        return width * getHeight();
    }
    public double perimeterRectangle() {
        return (width + getHeight()) * 2;
    }
    public double getWidth() {
        return width;
    }
}








