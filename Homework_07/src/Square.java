public class Square extends GeometricFigure {
    protected  double height;

    public Square(double coordinateX, double coordinateY, double height) {
        super(coordinateX, coordinateY);
        this.height = height;
    }
    public double areaSquare () {
        return height * height;
    }
    public double perimeterSquare () {
        return height * 4;
    }
    public double getHeight() {
        return height;
    }
}
