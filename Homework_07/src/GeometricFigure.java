
public abstract class GeometricFigure {
    protected double coordinateX;
    protected double coordinateY;

    public GeometricFigure(double coordinateX, double coordinateY) {
    }

    public void movingFigure(double x, double y) {
        double setCoordinateX = x;
        double setsetCoordinateY = y;
    }

    public void setCoordinateX(double coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setCoordinateY(double coordinateY) {
        this.coordinateY = coordinateY;
    }

    public double getCoordinateX() {
        return coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }
}



