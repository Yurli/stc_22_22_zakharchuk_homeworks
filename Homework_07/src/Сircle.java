public class Сircle extends GeometricFigure {
    protected double radius;
    protected double pi = 3.14;
    public Сircle(double coordinateX, double coordinateY, double radius) {
        super(coordinateX, coordinateY);
        this.radius = radius;

    }
    public double areaСircle() {
        return (int) (getPi() * (getRadius() * getRadius()));
    }
    public double perimeterСircle() {
        return (int) (2 * getPi() * getRadius());
    }
    public double getPi() {
        return pi;
    }
    public double getRadius() {
        return radius;
    }
}

