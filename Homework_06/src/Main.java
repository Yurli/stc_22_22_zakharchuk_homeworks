import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int Length = scanner.nextInt();
        int[] array = new int[Length];



        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();

        }
        int  i = 0, right = 0, left = 0, mid = 0, sum_local_min = 0;
        while (i != array.length) {
            left = mid;
            mid = right;
            right = array[i++];
            if (right > mid && mid < left) {
                System.out.println("local min: " + mid);
                sum_local_min++;
            }
        }
        if (i > 1 && right < mid) {
            System.out.println("local min: " + right);
            sum_local_min++;

        }
        System.out.println(Arrays.toString(array));
        System.out.println("Count of local min: " + sum_local_min);
    }




}