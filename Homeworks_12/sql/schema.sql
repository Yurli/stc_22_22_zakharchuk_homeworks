drop table if exists trip;
drop table if exists car;
drop table if exists account;


create table account (
                         id bigserial primary key,
                         first_name char (20) default 'DEFAULT_FIRSTNAME',
                         last_name char (20) default 'DEFAULT_LASTNAME',
                         telephone_number char (10) unique,
                         experience integer check ( experience >=3 ),
                         age integer check ( age >= 18 and age <=120 ) not null,
                         driver_license bool,
                         Rights_category char (5),
                         Rating integer check ( Rating >=0 and Rating <=5 )
);

create table car (
                     id bigserial primary key,
                     Model char (20) not null,
                     Colour char (20) default 'DEFAULT_COLOUR',
                     Number char (10) not null,
                     Owner_id integer

);

create table trip (
                      account_id bigint,
                      car_id bigint,
                      foreign key (account_id) references account (id),
                      foreign key (car_id) references car (id),
                      Date_of_trip timestamp,
                      Duration_of_the_trip time
);