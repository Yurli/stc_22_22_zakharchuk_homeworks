insert into account (telephone_number, experience, age, driver_license, Rights_category, Rating)
values (9999999999, 5, 20,true,'B',2);
insert into account (telephone_number, experience, age, driver_license, Rights_category, Rating)
values (9888888888, 10, 30,false,'BC',3);
insert into account (telephone_number, experience, age, driver_license, Rights_category, Rating)
values (9777777777, 3, 27,false,'BCD',2);
insert into account (telephone_number, experience, age, driver_license, Rights_category, Rating)
values (9666666666, 5, 31,true,'B',2);
insert into account (telephone_number, experience, age, driver_license, Rights_category, Rating)
values (955555555, 7, 32,true,'BCDE',5);

insert into car (model, colour, number, owner_id)
values ('Carina', 'Black', 'o111bb', 1),
       ('Almera', 'Gray', 'k222kk', 2),
       ('Astra', 'Red', 'm333mm', 3),
       ('Largus', 'Green', 'c444cc', 4),
       ('L-200', 'White', 'a555aa', 5);

insert into trip (account_id, car_id, Date_of_trip, Duration_of_the_trip)
values (1,1,'2022-11-11', '1:00'),
       (2,2, '2022-11-11', '1:35'),
       (3,3, '2022-11-12', '0:37'),
       (4,4, '2022-11-13', '2:01'),
       (5,5, '2022-11-15', '01:01');